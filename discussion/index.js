// console.log("Hello");



/*
		while loop
		 - takes a single condition. If the condition is true it will run the code.

		syntax:
		while(condition) {
			statement
		}

*/

let count = 5

while(count !== 0) {
	console.log("While: " + count);
	count--;
}

let num = 0

while(num <= 5) {
	console.log("While: " + num);
	num++;
}


console.log("MINI ACTIVITY");

let numA = 0
	while(numA <= 30) {
		console.log("While: " + numA)
		numA +=2;
	}


/*
	Do While Loop
		- a do while loop works a lot like the while loop. But unlike while loops, do-while loops guarantyee that the code will be executed at least once.
	
	Syntax:
	  do {
			statement
	  } while (expreesion/condition) 
*/

/*let number = Number(prompt("Gve me a number:"));

do {
	console.log("Do while: " + number);
	number++;
} while(number < 10);*/

/*let number = Number(prompt("Give me another number:"));

do {
	console.log("Do while: " + number);
	number--;
} while(number > 10);*/

/*
		For Loop
				- more flexible than the while loop and do while loop

				- part:
					- initial value: tracks the progress of the loop
					- condition: if true it will run the code; but if false it will stope the iteration/code
					- iteration: it indicates how to advance the loop(increasing or decreasing); final expression

		Syntax:
		 for(initialValue; condition; iteration) {
			statement
		 }
*/

for( let count = 0; count <= 20; count ++) {
	console.log("For Loop count: " + count);
}

let myString = "Mae Sargento";
console.log(myString.length);

console.log(" ");
console.log(myString[0]);
console.log(myString[10]);

console.log(" ");

for(let x = 0; x < myString.length; x++) {
	console.log(myString[x]);
}

console.log(" ");

let myName = "JOSEPHINE";

for(let n = 0; n < myName.length; n++) {
	
	if(
		myName[n].toLowerCase() == "a" ||
		myName[n].toLowerCase() == "e" ||
		myName[n].toLowerCase() == "i" ||
		myName[n].toLowerCase() == "o" ||
		myName[n].toLowerCase() == "u"
		){
			console.log("Vowel");
		 } else {
		 	console.log(myName[n].toLowerCase())
		 }
		}

console.log(" ");


let theWord = "extravagant";
let removeVowels = "";

for(let w = 0; w < theWord.length; w++) {
	
	if(
		theWord[w] == "a" ||
		theWord[w] == "e" ||
		theWord[w] == "i" ||
		theWord[w] == "o" ||
		theWord[w] == "u"
		) {
			console.log()
			continue;
			}  else {
				removeVowels += theWord[w];
			}
	}
	console.log("The consonants from " + theWord + " are " + removeVowels);

	/*
		Continue and Break Statement

		"continue" statement allows the code to go to the next iteration without finishing the execution of all the statements in the code block

		"break" statement on the other hand is a keyword that ends the execution of the code or the current loop.
	*/

console.log(" ");

	for (let count =0; count <= 20; count++) {
		if (count % 5 === 0) {
			console.log("Div by 5")
			continue;
		}

		console.log("continue and break: " + count)
		if (count > 10) {
			break;
		}
	}

console.log(" ");

let name = "Alexander"

for (let i = 0; i < name.length; i++) {
	console.log(name[i])

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the iteration.")
		continue;
	}

	if(name[i].toLowerCase() === "d") {
		console.log("Continue to the iteration")
		break;
	}
}